<?php

class Mail_Attachment
{
    const EOL = "\n";
    
    private $url;

    private $name; // Attachment name

    private $description; // Attachment description

    private $contentType = null; // MIME

    private $content = null; // Attachment body

    public function __construct($url, $name = '', $description = '') {
        $this->url = $url;
        $pathInfo = pathinfo(realpath($url));
        // If no name selected, it is written from the file
        $this->name = empty($name) ? $pathInfo['basename'] : $name;
        $this->description = $description;
        $this->contentType = mime_content_type($this->url);
        $this->content = chunk_split(
            base64_encode(
                file_get_contents($this->url)));
    }

    public function __toString()
    {
        $content = '';
        $content .= "Content-Type: " . $this->contentType . "; name=\"" . $this->name . "\"" . self::EOL;
        $content .= "Content-Transfer-Encoding: base64" . self::EOL;
        $content .= "Content-Description: " . $this->description . self::EOL;
        $content .= "Content-Disposition: attachment; filename=\"" . $this->name . "\"" . self::EOL . self::EOL;
        $content .= $this->content . self::EOL . self::EOL;
        return $content;
    }
}