<?php

class Mail_Content {

    const EOL = "\n";

    private $message = null;

    private $contentType = null;

    private $charset = 'iso-8859-1';

    public function __construct($contentType = 'text/plane') {
        $this->contentType = $contentType;
    }

    public function setContent($string, $charset = null) {
        $this->message = $string;
        if($charset) {
            $this->charset = $charset;
        }
    }

    public function isEmpty() {
        return $this->message;
    }

    public function __toString() {
        $message = "Content-Type: " . $this->contentType . "; charset=\"" . $this->charset . "\"" . self::EOL;
        $message .= "Content-Transfer-Encoding: 7bit" . self::EOL . self::EOL;
        $message .= $this->message . self::EOL . self::EOL;
        return $message;
    }
}