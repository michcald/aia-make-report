<?php

class Mail
{
    const EOL = "\n";
    
    private $from = null;

    private $replyTo = null;

    private $cc = array();

    private $bcc = array();

    private $mimeVersion = '1.0';

    private $contentType = 'multipart/alternative'; // multipart/alternative | multipart/mixed if attachments

    private $boundary = null;
	
    private $to = array();

    private $subject = null;

    /**
     *
     * @var Mail_Content
     */
    private $text = null;

    /**
     *
     * @var Mail_Content
     */
    private $html = null;

    private $attachments = array();

    public function __construct() {
        $this->boundary = md5(time());
        $this->text = new Mail_Content('text/plane');
        $this->html = new Mail_Content('text/html');
    }

    public function setFrom($email, $name = '') {
        $this->from = trim($name) . '<' . trim($email) . '>';
        return $this;
    }

    public function setReplyTo($email, $name = '') {
        $this->replyTo = trim($name) . '<' . trim($email) . '>';
        return $this;
    }

    public function addTo($email, $name = '') {
        $this->to[] = trim($name) . '<' . trim($email) . '>';
        return $this;
    }

    public function addCc($email, $name = '') {
        $this->cc[] = trim($name) . '<' . trim($email) . '>';
        return $this;
    }

    public function addBcc($email, $name = '') {
        $this->bcc[] = trim($name) . '<' . trim($email) . '>';
        return $this;
    }

    public function setSubject($subject) {
        $this->subject = trim(strip_tags($subject));
        return $this;
    }

    public function setBodyText($string, $charset = 'iso-8859-1') {
        $this->text->setContent($string, $charset);
        if($this->html->isEmpty()) {
            $this->setBodyHtml(nl2br($string));
        }
        return $this;
    }

    public function setBodyHtml($string, $charset = 'iso-8859-1') {
        $this->html->setContent($string, $charset);
        if($this->text->isEmpty()) {
            $this->setBodyText(
                str_replace(
                    array('<br />','<br/>','<br>'),
                    array("\n","\n","\n"),
                    $string
                    )
                );
        }
        
        return $this;
    }

    public function addAttachment($url, $name = '', $description = '') {
        $this->attachments[] = new Mail_Attachment($url, $name, $description);
        $this->contentType = 'multipart/mixed';
        return $this;
    }

    private function header() {
        $header = '';
        if($this->from != null) {
            $header .= "From: " . $this->from . self::EOL;
        }
        if($this->replyTo != null) {
            $header .= "Reply-To: " . $this->replyTo . self::EOL;
        }
        if($this->cc != null) {
            $header .= "Cc: " . implode(', ', $this->cc) . self::EOL;
        }
        if($this->bcc != null) {
            $header .= "Bcc: " . implode(', ', $this->bcc) . self::EOL;
        }
        $header .= "MIME-Version: " . $this->mimeVersion . self::EOL;
        $header .= "Content-Type: " . $this->contentType . "; ";
        $header .= "boundary=\"" . $this->boundary . "\"" . self::EOL . self::EOL;
        return $header;
    }

    public function send()
    {
        $boundary1 = $this->boundary;
        $body = '';
        if(count($this->attachments)) {
            $boundary2 = md5($boundary1);
            $body .= "--" . $boundary1 . self::EOL;
            // text message
            $body .= "Content-Type: multipart/alternative; boundary=\"" . $boundary2 . "\"" . self::EOL . self::EOL;
            $body .= "--" . $boundary2 . self::EOL;
            $body .= $this->text;
            $body .= "--" . $boundary2 . self::EOL;
            $body .= $this->html;
            $body .= "--" . $boundary2 . "--" . self::EOL;
            // Add the attachments
            $body .= "--" . $boundary1 . self::EOL;
            $body .= implode("--" . $boundary1 . self::EOL, $this->attachments);
            $body .= "--" . $boundary1 . "--" . self::EOL;
        }
        else { // no attachments
            $body .= "--" . $boundary1 . self::EOL;
            $body .= $this->text;
            $body .= "--" . $boundary1 . self::EOL;
            $body .= $this->html;
            $body .= "--" . $boundary1 . "--" . self::EOL;
        }
        $to = implode(', ', $this->to);
        return mail($to, $this->subject, $body, $this->header());
    }
}