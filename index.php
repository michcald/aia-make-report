<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="../css/reset.css" />
<style type="text/css">
    a,a:visited {
        color: #0000ff;
        text-decoration: none;
    }
    table {
        width: 90%;
        font-size: 13px;
        font-style: italic;
        text-align: center;
    }
    table td {
        width: 45%;
        padding: 10px;
    }
    table td:nth-child(1) {
        background-color: #C1D2EE;
    }
    table td:nth-child(2) {
        background-color: #DDDDDD;
    }
    table tr:first-child td:nth-child(1),table tr:first-child td:nth-child(2) {
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        font-style: normal;
    }
    table tr:nth-child(2) td:nth-child(1),table tr:nth-child(2) td:nth-child(2) {
        font-size: 15px;
        font-style: normal;
    }
</style>

<h1>AIA Make Report 1.1</h1>

<p>AIA Make Report vi permette di velocizzare e migliorare la compilazione
    dei rapporti di gara, sia delle categorie CRA che OTP ed infine vi offre un
    modo automatico per l’archiviazione degli stessi, senza dover per forza
    salvarli sul proprio pc.</p>

<h2>Cosa c'è di nuovo</h2>

<p>La nuova versione 1.1 permette di aggiungere un'immagine come firma, in questo
    modo non dovrete più stampare il rapporto, firmarlo, scannerizzarlo e spedirlo,
    ma basterà inoltrarlo direttamente via email.</p>
<p>Per una migliore integrazione della firma, <b>la dimensione ottimale dell'immagine
    da selezionare è di 200 pixel di larghezza e 50 pixel di altezza</b>. Consiglio
    vivamente di fare delle prove e, una volta raggiunta la dimensione voluta,
    salvate l'immagine sul vostro computer per riutilizzarla successivamente.</p>

<h2>Accedi al servizio</h2>

<table>
    <tr>
        <td>Rapporto gare OTP/SGS</td>
        <td>Rapporto gare CRA</td>
    </tr>
    <tr>
        <td><a href="otp/form.php">Compila</a></td>
        <td><a href="cra/form.php">Compila</a></td>
    </tr>
    <tr>
        <td>Giovanissimi - Allievi - Juniores - Terza cat. - Seconda cat.</td>
        <td>Prima cat. - Promozione - Eccellenza</td>
    </tr>
    <tr>
        <td></td>
        <td>Da spedire via fax al Giudice Sportivo FIGC della Delegazione di
            Brescia entro le 12:00 del giorno successivo alla gara al numero 030-3730716</td>
    </tr>
</table>

<h2>Privacy</h2>

<p style="font-weight: bold">Informativa ai sensi del decreto legislativo n° 193
    del 30 giugno 2003 codice in materia di protezione dei dati personali.</p>

<p>"Vi informiamo che i dati, le immagini, e qualsiasi altra informazione
    inserita nel modulo AIA Make Report verrà utilizzata solo nella composizione
    del report stesso. Non verrà in alcun modo memorizzata o riutilizzata per altri scopi."</p>

<h2>Guida Veloce</h2>

<ul>
    <li>selezionate il tipo di rapporto di gara (CRA o OTP) che dovete compilare
        cliccando su uno dei due link;</li>
    <li>si aprirà a questo punto una nuova pagina che vi mostra il classico
        rapporto di gara compilabile;</li>
    <li>riempite tutti i campi come se lo steste compilando a mano;</li>
    <li>una volta compilato cliccate sul pulsante a fondo pagina <i>Genera PDF</i>;</li>
    <li>aspettate qualche attimo e vi si aprirà una nuova finestra col vostro
        rapporto di gara compilato nel formato PDF;</li>
    <li>è importante a questo punto controllare che tutti i campi siano stati
        inseriti correttamente. (Se avete tralasciato qualcosa, potete notare
        che la finestra in cui avete compilato il rapporto è ancora aperta ed i
        campi contengono quello che avete inserito prima. Quindi modificate
        quello che c’è da modificare e poi ripetete la procedura cliccando ancora
        sul pulsante <i>Genera PDF</i>);</li>
</ul>