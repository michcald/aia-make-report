<style type="text/css">
    input[type=text] {
        text-align:center;
        background:#dddddd;
    }
    table td {
        padding: 1px 1px;
    }
</style>

<?php

function inputText($name, $top, $left, $width, $placeholder = '') {
    return "<input type=\"text\" name=\"$name\" id=\"$name\" style=\"text-align:center;position:absolute;top:{$top}px;left:{$left}px;width:{$width}px;background:#dddddd\" autocomplete=\"off\" placeholder=\"$placeholder\" />";
}

function textarea($name, $top, $left, $width, $height, $placeholder = '') {
    return "<textarea name=\"$name\" id=\"$name\" style=\"position:absolute;top:{$top}px;left:{$left}px;width:{$width}px;height:{$height}px;background:#dddddd\"  placeholder=\"$placeholder\"></textarea>";
}

?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="../css/reset.css" />
<link rel="stylesheet" href="css/layout.css" />

<img src="img/otp_page1.jpg" width="1000" />
<img src="img/otp_page2.jpg" width="1000" />

<form action="export.php" target="_new" method="post" enctype="multipart/form-data">

<!-- Intestazioni -->
<?=inputText('nome', 40, 750, 200, 'Caccamo, Felice')?>
<div style="position:absolute; top:80px; left:750px;">Brescia</div>

<!-- Dati Partita -->
<?=inputText('gara', 115, 45, 350, 'Squadra 1 - Squadra 2')?>
<?=inputText('campionato', 115, 420, 400, 'Categoria o torneo')?>
<?=inputText('durata', 115, 845, 110, '90\'')?>
<?=inputText('data', 160, 45, 110, date('d/m/Y'))?>
<?=inputText('citta', 160, 180, 640, 'Indirizzo del campo')?>
<?=inputText('ora', 160, 845, 110, date('H:i'))?>

<!-- Risultati -->
<?=inputText('societa1', 210, 190, 250, 'Società ospitante')?>
<?=inputText('nReti1', 210, 515, 50, '0')?>
<?=inputText('lettReti1', 210, 590, 120, 'zero')?>
<?=inputText('societa2', 270, 190, 250, 'Società Ospite')?>
<?=inputText('nReti2', 270, 515, 50, '0')?>
<?=inputText('lettReti2', 270, 590, 120, 'zero')?>

<?=textarea('listaReti_1T', 360, 150, 310, 50)?>
<?=textarea('listaReti_2T', 360, 665, 310, 50)?>

<!-- Orari -->
<?=inputText('inizio', 430, 180, 100)?>
<?=inputText('riposo', 459, 180, 100)?>
<?=inputText('fine', 488, 180, 100)?>

<?=inputText('rec_1T', 445, 875, 100)?>
<?=inputText('rec_2T', 475, 875, 100)?>


<!-- Sostituzioni -->
<!-- Squadra 1 -->
<?
for($i=0 ; $i<7 ; $i++)
{
	?>
<table style="position:absolute; top:<? echo 545 + ($i * 50) ?>px; left:40px;" cellpadding="0" cellspacing="0">
<tr>
	<td>
        al
        <input type="text" name="soc1_sostitAl<? echo $i ?>" style="width: 30px" />
        del
        <input type="text" name="soc1_sostitDel<? echo $i ?>" style="width: 30px" />
        t.
    </td>
    <td>esce n.</td>
    <td><input type="text" name="soc1_sostitEsce<? echo $i ?>" style="width: 270px" /></td>
</tr>
<tr>
	<td></td>
    <td>entra n.</td>
    <td><input type="text" name="soc1_sostitEntra<? echo $i ?>" style="width: 270px" /></td>
</table>
	<?
}
?>

<!-- Squadra 2 -->
<?
for($i=0 ; $i<7 ; $i++)
{
	?>
<table style="position:absolute; top:<? echo 545 + ($i * 50) ?>px; left:520px;" cellpadding="0" cellspacing="0">
<tr>
	<td>
        al
        <input type="text" name="soc2_sostitAl<? echo $i ?>" style="width: 30px" />
        del
        <input type="text" name="soc2_sostitDel<? echo $i ?>" style="width: 30px" />
        t.
    </td>
	<td>esce n.</td>
    <td><input type="text" name="soc2_sostitEsce<? echo $i ?>" style="width: 270px" /></td>
</tr>
<tr>
	<td></td>
    <td>entra n.</td>
    <td><input type="text" name="soc2_sostitEntra<? echo $i ?>" style="width: 270px" /></td>
</table>
	<?
}
?>

<!-- Persone ammesse nel recinto di gioco -->
<?=inputText('dirig_soc1', 930, 230, 230)?>
<?=inputText('medico_soc1', 960, 230, 230)?>
<?=inputText('allen_soc1', 993, 230, 230)?>
<?=inputText('massag_soc1', 1023, 230, 230)?>

<?=inputText('dirig_soc2', 930, 720, 230)?>
<?=inputText('medico_soc2', 960, 720, 230)?>
<?=inputText('allen_soc2', 993, 720, 230)?>
<?=inputText('massag_soc2', 1023, 720, 230)?>

<!-- Misure d'ordine -->
<?=textarea('misureOrdine', 1090, 430, 530, 50, 'A cura della società ospitante')?>

<!-- Comportamento dirigenti -->
<?=textarea('comportDirig', 1180, 50, 900, 250, 'Nulla da segnalare')?>

<!-- Comportamento del pubblico -->
<?=textarea('comportPubb', 1490, 50, 900, 250, 'Nulla da segnalare')?>

<!-- Espulsi -->
<?=textarea('espulsi', 1800, 50, 900, 250, 'Nessuno')?>

<!-- Ammoniti -->
<?=textarea('ammoniti', 2150, 50, 900, 250, 'Nessuno')?>

<!-- Varie -->
<?=textarea('varie', 2500, 50, 900, 50, 'Nulla da segnalare')?>

<!-- Assistenti  -->
<?=inputText('assistente1', 2570, 230, 320, 'Assistente 1 (Nome società)')?>
<?=inputText('assistente2', 2645, 230, 320, 'Assistente 2 (Nome società)')?>

<!-- Telefono -->
<?=inputText('tel1', 2717, 785, 150, 'Cellulare')?>
<?=inputText('tel2', 2745, 785, 150, 'Casa')?>

<input type="file" name="firma" style="position:absolute;top:2600px;left:635px;background: #ff6666;padding:20px 5px" />

<p style="text-align: center;padding: 40px 0;">
    Riempi il campo seguente se vuoi ricevere il rapporto via email:<br /><br />
    <input type="text" name="email" placeholder="mia@email.it" />
<p>

<br /><br />
<center><input type="submit" name="generatePDF" value="Genera PDF" /></center>
<br /><br />

</form>
