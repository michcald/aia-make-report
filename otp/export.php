<?
include '../lib/fpdf.class/fpdf.php';
include '../lib/Image.php';
include '../lib/Mail.php';
include '../lib/Mail/Attachment.php';
include '../lib/Mail/Content.php';

$myName = stripslashes($_POST['nome']);


$societa1 = stripslashes($_POST['societa1']);
$societa2 = stripslashes($_POST['societa2']);
//////////////////////////////////////////////////

$pdf = new FPDF();
$pdf -> AddPage();
$pdf -> SetFont('Arial');

//////////////////////////////////////////////////
// Settaggio informazioni pdf
//////////////////////////////////////////////////
$pdf -> SetTitle("Rapporto di gara SGS");
$pdf -> SetSubject ("Rapporto di gara SGS generato mediante script PHP by Michael Caldera");
$pdf -> SetCreator("Michael Caldera");
$pdf -> SetAuthor("Michael Caldera");

//////////////////////////////////////////////////
// Intestazioni
//////////////////////////////////////////////////
$pdf -> SetFontSize(12);

//////////////////////////////////////////////////
// Dettaglia arbitro, partita
//////////////////////////////////////////////////
$pdf -> SetLineWidth(0.5);
$pdf -> Rect(6, 6, 199, 36);
// COMITATO REG LOMBARDIA
$pdf -> Rect(6, 6, 64, 16);
$pdf -> SetFont('Arial', 'B', 12);
$pdf -> Text(14, 13, 'COMITATO REGIONALE');
$pdf -> Text(26, 18, 'LOMBARDIA');

// FIGC
$pdf -> Rect(70, 6, 71, 16);
$pdf -> Image('img/logoFIGC.jpg', 84, 7);
$pdf -> SetFont('Arial', 'I', 10);
$pdf -> Text(99, 12, 'FEDERAZIONE');
$pdf -> Text(99, 16, 'ITALIANA');
$pdf -> Text(99, 20, 'GIUOCO CALCIO');

// Arbitro
$pdf -> Rect(141, 6, 64, 16);
$pdf -> SetFont('Arial', '', 8);
$pdf -> Text(142, 12, 'Arbitro Sig.');
$pdf -> SetFontSize(11);
$pdf -> Text(157, 12, $myName);
$pdf -> SetFontSize(8);
$pdf -> Text(142, 19, 'Sez. AIA di');
$pdf -> SetFontSize(11);
$pdf -> Text(157, 19, 'Brescia');

// Indicaz gara
$pdf -> Rect(6, 22, 80, 10);

$pdf -> SetFontSize(7);
$pdf -> Text(34, 31, 'indicazione della gara');
$pdf -> SetFontSize(10);
$pdf -> Text(8, 27, stripslashes($_POST['gara']));

// campionato torneo
$pdf -> Rect(86, 22, 90, 10);

$pdf -> SetFontSize(7);
$pdf -> Text(111, 31, 'indicazione del campionato o torneo');
$pdf -> SetFontSize(10);
$pdf -> Text(89, 27, stripslashes($_POST['campionato']));

// durata
$pdf -> Rect(176, 22, 29, 10);

$pdf -> SetFontSize(7);
$pdf -> Text(181, 31, 'durata della gara');
$pdf -> SetFontSize(10);
$pdf -> Text(187, 27, stripslashes($_POST['durata']));

// data
$pdf -> Rect(6, 32, 29, 10);

$pdf -> SetFontSize(7);
$pdf -> Text(12, 41, 'data della gara');
$pdf -> SetFontSize(10);
$pdf -> Text(10, 38, $_POST['data']);

// campo e citt�
$pdf -> Rect(35, 32, 141, 10);

$pdf -> SetFontSize(7);
$pdf -> Text(87, 41, 'citta\' e campo della gara - indirizzo');
$pdf -> SetFontSize(10);
$pdf -> Text(37, 38, stripslashes($_POST['citta']));

// orario
$pdf -> Rect(6, 32, 29, 10);

$pdf -> SetFontSize(7);
$pdf -> Text(181, 41, 'orario della gara');
$pdf -> SetFontSize(10);
$pdf -> Text(184, 38, $_POST['ora']);


//////////////////////////////////////////////////
// Risultati
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 11);
$pdf -> Text(23, 57, 'RISULTATO');
$pdf -> Image('img/cra_graffaBig.jpg', 48, 44);

$pdf -> SetFont('Arial', '', 7);
$pdf -> Text(75, 52, '(societa\' ospitante)');
$pdf -> Text(75, 65, '(societa\' ospitata)');

$pdf -> SetFontSize(7);
$pdf -> Text(136, 52, '(cifra)');
$pdf -> Text(136, 65, '(cifra)');
$pdf -> Text(153, 52, '(lettere)');
$pdf -> Text(153, 65, '(lettere)');

$pdf -> SetFontSize(12);
$pdf -> Text(58, 49, stripslashes($societa1));
$pdf -> Text(58, 62, stripslashes($societa2));

$pdf -> Text(120, 49, 'reti');
$pdf -> Text(120, 62, 'reti');

$pdf -> Text(136, 49, stripslashes($_POST['nReti1']));
$pdf -> Text(153, 49, stripslashes($_POST['lettReti1']));
$pdf -> Text(136, 62, stripslashes($_POST['nReti2']));
$pdf -> Text(153, 62, stripslashes($_POST['lettReti2']));

// Minuti delle reti
$pdf -> SetFont('Arial', 'B', 12);
$pdf -> Text(55, 72, 'Reti segnate (indicare societa\' e minuto di giuoco)');
$pdf -> SetFont('Arial', '', 12);
// 1� tempo
$pdf -> Rect(7, 75, 95, 14);
$pdf -> SetFontSize(10);
$pdf -> Text(8, 79, 'Primo tempo:');

$listaReti_1T = stripslashes($_POST['listaReti_1T']);
$pdf -> SetXY(30, 76);
$pdf -> MultiCell(66, 4, $listaReti_1T, 0, 'L');

// 2� tempo
$pdf -> Rect(109, 75, 95, 14);
$pdf -> Text(110, 79, 'Secondo tempo:');

$listaReti_2T = stripslashes($_POST['listaReti_2T']);
$pdf -> SetXY(136, 76);
$pdf -> MultiCell(66, 4, $listaReti_2T, 0, 'L');

$pdf -> SetLineWidth(1);
$pdf -> Line(106, 75, 106, 90);
$pdf -> Line(6, 90, 205, 90);
$pdf -> SetLineWidth(0.5);
//////////////////////////////////////////////////
// Orari e tempi
//////////////////////////////////////////////////
$pdf -> Text(7, 95, 'Ora di inizio:');
$pdf -> Text(50, 95, $_POST['inizio']);
$pdf -> Text(7, 101, 'Durata del riposo:');
$pdf -> Text(50, 101, stripslashes($_POST['riposo']));
$pdf -> Text(7, 107, 'Ora della fine:');
$pdf -> Text(50, 107, $_POST['fine']);

$pdf -> Text(103, 100, 'Minuti neutralizzati');
$pdf -> Image('img/cra_graffaSmall.jpg', 137, 90.6);
$pdf -> Text(145, 98, 'nel primo tempo min.');
$pdf -> Text(190, 98, stripslashes($_POST['rec_1T']));
$pdf -> Text(145, 104, 'nel secondo tempo min.');
$pdf -> Text(190, 104, stripslashes($_POST['rec_2T']));

$pdf -> SetLineWidth(1);
$pdf -> Line(6, 110.4, 205, 110.4);
$pdf -> SetLineWidth(0.5);

//////////////////////////////////////////////////
// Sostituzioni
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 11);
$pdf -> Text(79, 114, 'EVENTUALI SOSTITUZIONI');
$pdf -> SetFont('Arial', '', 10);

for($i=0 ; $i<7 ; $i++)
{
	$pdf -> Text(7, 119+($i * 11), 'al');
	$pdf -> Text(13, 119+($i * 11), stripslashes($_POST['soc1_sostitAl'.$i]));
	$pdf -> Text(19, 119+($i * 11), 'del');
	$pdf -> Text(26, 119+($i * 11), $_POST['soc1_sostitDel'.$i]);
	$pdf -> Text(32, 119+($i * 11), 't.');
	$pdf -> Text(37, 119+($i * 11), 'esce n.');
	$pdf -> Text(50, 119+($i * 11), $_POST['soc1_sostitEsce'.$i]);
	$pdf -> Text(37, 124+($i * 11), 'entra n.');
	$pdf -> Text(50, 124+($i * 11), $_POST['soc1_sostitEntra'.$i]);
	$pdf -> SetFontSize(10);
}
for($i=0 ; $i<7 ; $i++)
{
	$pdf -> Text(108, 119+($i * 11), 'al');
	$pdf -> Text(114, 119+($i * 11), stripslashes($_POST['soc2_sostitAl'.$i]));
	$pdf -> Text(120, 119+($i * 11), 'del');
	$pdf -> Text(127, 119+($i * 11), $_POST['soc2_sostitDel'.$i]);
	$pdf -> Text(133, 119+($i * 11), 't.');
	$pdf -> Text(138, 119+($i * 11), 'esce n.');
	$pdf -> Text(151, 119+($i * 11), $_POST['soc2_sostitEsce'.$i]);
	$pdf -> Text(138, 124+($i * 11), 'entra n.');
	$pdf -> Text(151, 124+($i * 11), $_POST['soc2_sostitEntra'.$i]);
	$pdf -> SetFontSize(10);
}

$pdf -> Line(106, 116, 106, 193);
$pdf -> Line(7, 193, 205, 193);

//////////////////////////////////////////////////
// Dirigenti
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(55, 197, 'NOME DELLE PERSONE AMMESSE SUL TERRENO DI GIOCO');
$pdf -> SetFont('Arial', '', 10);
// ospitante
$pdf -> Text(40, 201, 'Societa\' ospitante');
$pdf -> Text(7, 207, 'Dirigente respons. Sig.');
$pdf -> Text(45, 207, stripslashes($_POST['dirig_soc1']));
$pdf -> Text(7, 214, 'Medico Sig.');
$pdf -> Text(45, 214, stripslashes($_POST['medico_soc1']));
$pdf -> Text(7, 221, 'Allenatore Sig.');
$pdf -> Text(45, 221, stripslashes($_POST['allen_soc1']));
$pdf -> Text(7, 228, 'Massaggiatore Sig.');
$pdf -> Text(45, 228, stripslashes($_POST['massag_soc1']));
// Ospite
$pdf -> Text(142, 201, 'Societa\' ospitata');
$pdf -> Text(109, 207, 'Dirigente respons. Sig.');
$pdf -> Text(146, 207, stripslashes($_POST['dirig_soc2']));
$pdf -> Text(109, 214, 'Medico Sig.');
$pdf -> Text(146, 214, stripslashes($_POST['medico_soc2']));
$pdf -> Text(109, 221, 'Allenatore Sig.');
$pdf -> Text(146, 221, stripslashes($_POST['allen_soc2']));
$pdf -> Text(109, 228, 'Massaggiatore Sig.');
$pdf -> Text(146, 228, stripslashes($_POST['massag_soc2']));

$pdf -> Line(106, 200, 106, 230);
$pdf -> Line(7, 230, 203, 230);

//////////////////////////////////////////////////
// Misure d'ordine
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 234, "Misure d'ordine prese dalla societa':");
$pdf -> SetFont('Arial', '', 10);
$pdf -> SetLineWidth(0.3);

$misureOrdine = stripslashes($_POST['misureOrdine']);
$pdf -> Rect(7,235, 196, 10);

$pdf -> SetXY(7, 235);
$pdf -> MultiCell(196, 4, $misureOrdine, 0, 'L');

//////////////////////////////////////////////////
// Comportamento dirigenti
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 250, "Comportamento delle persone ammesse sul terreno di giuoco:");
$pdf -> SetFont('Arial', '', 10);

$comportDirig = stripslashes($_POST['comportDirig']);
$pdf -> Rect(7, 251, 196, 27);

$pdf -> SetXY(7, 252);
$pdf -> MultiCell(196, 4, $comportDirig, 0, 'L');


// FINE PAGINA 1


// INIZIO PAGINA 2
$pdf -> AddPage();

//////////////////////////////////////////////////
// Comportamento pubblico
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 9, "Comportamento del pubblico, eventuali incidenti, osservazioni varie:");
$pdf -> SetFont('Arial', '', 10);

$comportPubb = stripslashes($_POST['comportPubb']);
$pdf -> Rect(7, 10, 196, 63);
$pdf -> SetXY(7, 10);
$pdf -> MultiCell(196, 4, $comportPubb, 0, 'L');

//////////////////////////////////////////////////
// Espulsioni
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 79, "Giocatori espulsi (minuto - Cognome e Nome - Data di Nascita - n. maglia - Societa' - motivazione)");
$pdf -> SetFont('Arial', '', 10);

$espulsi = stripslashes($_POST['espulsi']);
$pdf -> Rect(7, 80, 196, 67);

$pdf -> SetXY(7, 80);
$pdf -> MultiCell(196, 4, $espulsi, 0, 'L');

//////////////////////////////////////////////////
// Ammonizioni
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 152, "Giocatori ammoniti (minuto - Cognome e Nome - Data di Nascita - n. maglia - Societa' - motivazione)");
$pdf -> SetFont('Arial', '', 10);

$ammoniti = stripslashes($_POST['ammoniti']);
$pdf -> Rect(7, 153, 196, 67);

$pdf -> SetXY(7, 153);
$pdf -> MultiCell(196, 4, $ammoniti, 0, 'L');

//////////////////////////////////////////////////
// Varie
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 225, "Eventuali osservazioni sul campo di giuoco, spogliatoi, ecc.");
$pdf -> SetFont('Arial', '', 10);

$varie = stripslashes($_POST['varie']);
$pdf -> Rect(7, 226, 196, 16);

$pdf -> SetXY(7, 226);
$pdf -> MultiCell(196, 4, $varie, 0, 'L');

//////////////////////////////////////////////////
// Assistenti
//////////////////////////////////////////////////
$pdf -> Text(9, 257, 'ASSISTENTI');
$pdf -> Image('img/cra_graffaBig.jpg', 32, 244);
$pdf -> Text(42, 250, '1');
$pdf -> Text(45, 250, stripslashes($_POST['assistente1']));
$pdf -> Text(42, 265, '2');
$pdf -> Text(45, 265, stripslashes($_POST['assistente2']));

//////////////////////////////////////////////////
// Firma
//////////////////////////////////////////////////
if(isset($_FILES['firma']) && $_FILES['firma']['tmp_name'] != '') {
    $tmpName = '../tmp/' . time() . rand(0, 10000000) . '.jpg';
    $img = new Image($_FILES['firma']['tmp_name']);
    $img->adaptAndCut(200, 50)->save($tmpName);
    $pdf->Image($tmpName, 130, 246);
    unlink($tmpName);
}
$pdf -> Line(130, 264, 200, 264);
$pdf -> Text(156, 267, "L'ARBITRO");
$pdf -> SetFontSize(8);
$pdf -> Text(156, 270, "(firma leggibile)");

//////////////////////////////////////////////////
// Numeri di telefono
//////////////////////////////////////////////////
$pdf -> SetLineWidth(1);
$pdf -> Rect(133, 274, 65, 13);
$pdf -> SetFont('Arial', 'B', 9);
$pdf -> Text(134, 278, "Numero di Casa:");
$pdf -> SetFont('Arial', '', 9);
$pdf -> Text(162, 278, $_POST['tel1']);
$pdf -> SetFont('Arial', 'B', 9);
$pdf -> Text(134, 284, "Numero di Cell.:");
$pdf -> SetFont('Arial', '', 9);
$pdf -> Text(162, 284, $_POST['tel2']);

// Mostro a video il pdf
$pdf -> Output();

// SPEDISCO IN ALLEGATO AD UNA MAIL IL PDF
// Salvo come str per allegare alla mail e spedirla
$str = $pdf -> Output('', 'S');	// Salvo la stringa del pdf

if(isset($_POST['email'])) {
    $date = $_POST['garaDel'] ? $_POST['garaDel'] : date('d_m_Y');
    $societa1 = $_POST['societa1'];
    $societa2 = $_POST['societa2'];
    $m = new Mail();
    $m->addTo($_POST['email']);
    $m->setFrom("noreply@aiabrescia.com", "AIA BRESCIA");
    $m->setSubject("Rapporto del $date della gara $societa1 - $societa2");
    $text = "Caro collega,<br><br>";
    $text .= "In allegato il rapporto da te compilato col servizio <b>AIAMakeReport</b> ";
    $text .= "di <a href='http://www.aiabrescia.com'>aiabrescia.com</a> ";
    $text .= "della gara $societa1 - $societa2<br><br><br>";
    $text .= "<i>AIA Brescia WebMaster</i>";
    $m->setBodyHtml($text);

    $fileName = "{$date}_{$societa1}_$societa2.pdf";
    file_put_contents($fileName, $str);
    $m->addAttachment($fileName);
    $m->send();
    unlink($fileName);
}

?>
