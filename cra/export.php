<?
include '../lib/fpdf.class/fpdf.php';
include '../lib/Image.php';
include '../lib/Mail.php';
include '../lib/Mail/Attachment.php';
include '../lib/Mail/Content.php';

$myName = stripslashes($_POST['nome']);

$societa1 = stripslashes($_POST['societa1']);
$societa2 = stripslashes($_POST['societa2']);
//////////////////////////////////////////////////

$pdf = new FPDF();
$pdf -> AddPage();
$pdf -> SetFont('Arial');

//////////////////////////////////////////////////
// Settaggio informazioni pdf
//////////////////////////////////////////////////
$pdf -> SetTitle("Rapporto di gara CRA");
$pdf -> SetSubject ("Rapporto di gara CRA generato mediante script PHP by Michael Caldera");
$pdf -> SetCreator("Michael Caldera");
$pdf -> SetAuthor("Michael Caldera");

//////////////////////////////////////////////////
// Intestazioni
//////////////////////////////////////////////////
$pdf -> SetFontSize(12);
$pdf -> Text(39, 8, 'F.I.G.C.');
$pdf -> SetFont('Arial', 'B', 12);
$pdf -> Text(17, 14, 'LEGA NAZIONALE DILETTANTI');
$pdf -> Text(150, 8, 'COMITATO REGIONALE');
$pdf -> Text(160, 14, 'LOMBARDIA');

$pdf -> SetFont('Arial', '', 10);
$pdf -> Text(7, 23, 'Campionato:');
$pdf -> Text(30, 23, $_POST['campionato']);
$pdf -> Text(93, 23, 'Girone:');
$pdf -> Text(115, 23, $_POST['girone']);

//////////////////////////////////////////////////
// Dettaglia arbitro, partita
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 33, "Rapporto dell'arbitro Sig.");
$pdf -> SetFont('Arial', '', 10);
$pdf -> Text(53, 33, $myName);
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(135, 33, 'Sezione di:');
$pdf -> SetFont('Arial', '', 10);
$pdf -> Text(157, 33, 'Brescia');

$pdf -> Text(7, 43, 'Gara:');
$pdf -> Text(17, 43, stripslashes($_POST['gara']));
$pdf -> Text(125, 43, 'del:');
$pdf -> Text(139, 43, $_POST['garaDel']);
$pdf -> Text(166, 43, 'ore:');
$pdf -> Text(183, 43, $_POST['garaOre']);

$pdf -> Text(7, 53, 'disputata a:');
$pdf -> Text(28, 53, stripslashes($_POST['disputata']));
$pdf -> Text(109, 53, 'campo:');
$pdf -> Text(123, 53, stripslashes($_POST['campo']));

$pdf -> SetLineWidth(1);
$pdf -> Line(7, 57, 203, 57);

//////////////////////////////////////////////////
// Risultati
//////////////////////////////////////////////////
$pdf -> Text(7, 73, 'RISULTATO');
$pdf -> Image('img/cra_graffaBig.jpg', 29, 60);
// Numero reti societ� di casa
$pdf -> Text(40, 67, $societa1);
$pdf -> SetFontSize(6);
$pdf -> Text(60, 70, '(Societa\' ospitante)');
$pdf -> SetFontSize(10);
$pdf -> Text(103, 67, 'reti:');
$pdf -> Text(112, 67, $_POST['nReti1']);
$pdf -> SetFont('Arial', '', 8);
$pdf -> Text(120, 67, $_POST['lettReti1']);
$pdf -> SetFont('Arial', '', 10);
// Numero reti societa ospitata
$pdf -> Text(40, 80, $societa2);
$pdf -> SetFontSize(6);
$pdf -> Text(60, 83, '(Societ\' ospitata)');
$pdf -> SetFontSize(10);
$pdf -> Text(103, 80, 'reti:');
$pdf -> Text(112, 80, $_POST['nReti2']);
$pdf -> SetFont('Arial', '', 8);
$pdf -> Text(120, 80, $_POST['lettReti2']);
$pdf -> SetFont('Arial', '', 10);
// Minuti delle reti
$pdf -> SetFont('Arial', 'B', 9);
$pdf -> Text(40, 92, 'Reti segnate (indicare societa\' e minuto di gioco)');
$pdf -> SetFont('Arial', '', 10);
// 1 tempo
$pdf -> Text(7, 98, 'I. Tempo:');
$listaReti_1T = stripslashes($_POST['listaReti_1T']);
$pdf -> SetXY(22, 95);
$pdf -> MultiCell(48, 4, $listaReti_1T, 0, 'L');

// 2 tempo
$pdf -> Text(73, 98, 'II. Tempo:');
$listaReti_2T = stripslashes($_POST['listaReti_2T']);
$pdf -> SetXY(89, 95);
$pdf -> MultiCell(48, 4, $listaReti_2T, 0, 'L');

//////////////////////////////////////////////////
// Box caso di sospensione
//////////////////////////////////////////////////
$pdf -> Rect(141, 60, 63, 45);
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(142, 67, 'Non iniziata per');
$pdf -> SetFont('Arial', '', 10);
$nonIniziata = stripslashes($_POST['nonIniziata']);
$pdf -> SetXY(142, 68);
$pdf -> MultiCell(60, 4, $nonIniziata, 0, 'L');

$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(142, 90, 'Sospesa al');
$pdf -> SetFont('Arial', '', 10);
$pdf -> Text(165, 90, stripslashes($_POST['sospesaAl']));
$pdf -> Text(173, 90, 'del');
$pdf -> Text(185, 90, $_POST['sospesaTempo']);
$pdf -> Text(192, 90, 't. per');
$sospesa = stripslashes($_POST['sospesa']);
$pdf -> SetXY(142, 91);
$pdf -> MultiCell(60, 4, $sospesa, 0, 'L');

$pdf -> SetLineWidth(1);
$pdf -> Line(7, 108, 203, 108);

//////////////////////////////////////////////////
// Orari e tempi
//////////////////////////////////////////////////
$pdf -> Text(7, 112, 'Ora di inizio:');
$pdf -> Text(50, 112, $_POST['inizio']);
$pdf -> Text(7, 118, 'Durata del riposo:');
$pdf -> Text(50, 118, stripslashes($_POST['riposo']));
$pdf -> Text(7, 124, 'Ora della fine:');
$pdf -> Text(50, 124, $_POST['fine']);

$pdf -> Text(98, 119, 'Minuti neutralizzati');
$pdf -> Image('img/cra_graffaSmall.jpg', 130, 109);
$pdf -> Text(140, 112, 'nel 1 t.');
$pdf -> Text(155, 112, stripslashes($_POST['rec_1T']));
$pdf -> Text(163, 112, 'min. per');
$pdf -> Text(178, 112, $_POST['recPer_1T']);

$pdf -> Text(140, 125, 'nel 2 t.');
$pdf -> Text(155, 125, stripslashes($_POST['rec_2T']));
$pdf -> Text(163, 125, 'min. per');
$pdf -> Text(178, 125, $_POST['recPer_2T']);

$pdf -> Line(7, 128, 203, 128);

//////////////////////////////////////////////////
// Sostituzioni
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(8, 132, 'EVENTUALI VARIAZIONI NELLE FORMAZIONI DELLE SQUADRE');
$pdf -> SetFont('Arial', '', 10);

for($i=0 ; $i<5 ; $i++)
{
	$pdf -> Text(7, 137+($i * 11), 'al');
	$pdf -> Text(13, 137+($i * 11), stripslashes($_POST['soc1_sostitAl'.$i]));
	$pdf -> Text(19, 137+($i * 11), 'del');
	$pdf -> Text(26, 137+($i * 11), $_POST['soc1_sostitDel'.$i]);
	$pdf -> Text(32, 137+($i * 11), 't.');
	$pdf -> Text(37, 137+($i * 11), 'esce n.');
	$pdf -> Text(50, 137+($i * 11), $_POST['soc1_sostitEsce'.$i]);
	$pdf -> Text(37, 142+($i * 11), 'entra n.');
	$pdf -> Text(50, 142+($i * 11), $_POST['soc1_sostitEntra'.$i]);
	$pdf -> SetFontSize(10);
}
for($i=0 ; $i<5 ; $i++)
{
	$pdf -> Text(108, 137+($i * 11), 'al');
	$pdf -> Text(114, 137+($i * 11), stripslashes($_POST['soc2_sostitAl'.$i]));
	$pdf -> Text(120, 137+($i * 11), 'del');
	$pdf -> Text(127, 137+($i * 11), $_POST['soc2_sostitDel'.$i]);
	$pdf -> Text(133, 137+($i * 11), 't.');
	$pdf -> Text(138, 137+($i * 11), 'esce n.');
	$pdf -> Text(151, 137+($i * 11), $_POST['soc2_sostitEsce'.$i]);
	$pdf -> Text(138, 142+($i * 11), 'entra n.');
	$pdf -> Text(151, 142+($i * 11), $_POST['soc2_sostitEntra'.$i]);
	$pdf -> SetFontSize(10);
}

$pdf -> Line(105, 133, 105, 188);
$pdf -> Line(7, 189, 203, 189);

//////////////////////////////////////////////////
// Dirigenti
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(8, 193, 'NOME DELLE PERSONE AMMESSE SUL TERRENO DI GIOCO');
$pdf -> SetFont('Arial', '', 10);
// ospitante
$pdf -> Text(23, 200, 'Societa\':');
$pdf -> Text(45, 200, stripslashes($societa1));
$pdf -> Text(7, 207, 'Dirigente respons. Sig.');
$pdf -> Text(45, 207, stripslashes($_POST['dirig_soc1']));
$pdf -> Text(7, 214, 'Medico Sig.');
$pdf -> Text(45, 214, stripslashes($_POST['medico_soc1']));
$pdf -> Text(7, 221, 'Allenatore Sig.');
$pdf -> Text(45, 221, stripslashes($_POST['allen_soc1']));
$pdf -> Text(7, 228, 'Massaggiatore Sig.');
$pdf -> Text(45, 228, stripslashes($_POST['massag_soc1']));
// Ospite
$pdf -> Text(125, 200, 'Societa\':');
$pdf -> Text(146, 200, stripslashes($societa2));
$pdf -> Text(109, 207, 'Dirigente respons. Sig.');
$pdf -> Text(146, 207, stripslashes($_POST['dirig_soc2']));
$pdf -> Text(109, 214, 'Medico Sig.');
$pdf -> Text(146, 214, stripslashes($_POST['medico_soc2']));
$pdf -> Text(109, 221, 'Allenatore Sig.');
$pdf -> Text(146, 221, stripslashes($_POST['allen_soc2']));
$pdf -> Text(109, 228, 'Massaggiatore Sig.');
$pdf -> Text(146, 228, stripslashes($_POST['massag_soc2']));

$pdf -> Line(105, 200, 105, 230);
$pdf -> Line(7, 230, 203, 230);

//////////////////////////////////////////////////
// Misure d'ordine
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 234, "Misure d'ordine prese dalla societa\':");
$pdf -> SetFont('Arial', '', 10);
$pdf -> SetLineWidth(0.3);

$misureOrdine = stripslashes($_POST['misureOrdine']);
$pdf -> Rect(7,235, 196, 10);

$pdf -> SetXY(7, 235);
$pdf -> MultiCell(196, 4, $misureOrdine, 0, 'L');

//////////////////////////////////////////////////
// Comportamento dirigenti
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 250, "Comportamento dei dirigenti:");
$pdf -> SetFont('Arial', '', 10);

$comportDirig = stripslashes($_POST['comportDirig']);
$pdf -> Rect(7, 251, 196, 27);

$pdf -> SetXY(7, 252);
$pdf -> MultiCell(196, 4, $comportDirig, 0, 'L');


// FINE PAGINA 1


// INIZIO PAGINA 2
$pdf -> AddPage();

//////////////////////////////////////////////////
// Comportamento pubblico
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 9, "Comportamento del pubblico, eventuali incidenti:");
$pdf -> SetFont('Arial', '', 10);

$comportPubb = stripslashes($_POST['comportPubb']);
$pdf -> Rect(7, 10, 196, 24);
$pdf -> SetXY(7, 10);
$pdf -> MultiCell(196, 4, $comportPubb, 0, 'L');

//////////////////////////////////////////////////
// Espulsioni
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 38, "Giocatori espulsi");
$pdf -> Text(7, 42, "(minuto - Cognome e Nome - Data di Nascita - n. maglia - Societa\' - motivazione)");
$pdf -> SetFont('Arial', '', 10);

$espulsi = stripslashes($_POST['espulsi']);
$pdf -> Rect(7, 43, 196, 77);

$pdf -> SetXY(7, 43);
$pdf -> MultiCell(196, 4, $espulsi, 0, 'L');

//////////////////////////////////////////////////
// Ammonizioni
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 124, "Giocatori ammoniti");
$pdf -> Text(7, 128, "(minuto - Cognome e Nome - Data di Nascita - n. maglia - Societa' - motivazione)");
$pdf -> SetFont('Arial', '', 10);

$ammoniti = stripslashes($_POST['ammoniti']);
$pdf -> Rect(7, 129, 196, 77);

$pdf -> SetXY(7, 129);
$pdf -> MultiCell(196, 4, $ammoniti, 0, 'L');

//////////////////////////////////////////////////
// Varie
//////////////////////////////////////////////////
$pdf -> SetFont('Arial', 'B', 10);
$pdf -> Text(7, 210, "VARIE (eventuali osservazioni sul terreno di giuoco, spogliatoi, ecc.)");
$pdf -> SetFont('Arial', '', 10);

$varie = stripslashes($_POST['varie']);
$pdf -> Rect(7, 211, 196, 18);

$pdf -> SetXY(7, 211);
$pdf -> MultiCell(196, 4, $varie, 0, 'L');

//////////////////////////////////////////////////
// Assistenti
//////////////////////////////////////////////////
$pdf -> Text(9, 247, 'ASSISTENTI');
$pdf -> Image('img/cra_graffaBig.jpg', 32, 234);
$pdf -> Text(42, 240, '1');
$pdf -> Text(45, 240, stripslashes($_POST['assistente1']));
$pdf -> Text(42, 255, '2');
$pdf -> Text(45, 255, stripslashes($_POST['assistente2']));

//////////////////////////////////////////////////
// Firma
//////////////////////////////////////////////////
if(isset($_FILES['firma']) && $_FILES['firma']['tmp_name'] != '') {
    $tmpName = '../tmp/' . time() . rand(0, 10000000) . '.jpg';
    $img = new Image($_FILES['firma']['tmp_name']);
    $img->adaptAndCut(200, 50)->save($tmpName);
    $pdf->Image($tmpName, 130, 246);
    unlink($tmpName);
}

//////////////////////////////////////////////////
// Numeri di telefono
//////////////////////////////////////////////////
$pdf -> Line(130, 267, 200, 267);
$pdf -> Text(156, 270, "L'ARBITRO");
$pdf -> SetFontSize(8);
$pdf -> Text(156, 273, "(firma leggibile)");
$pdf -> SetLineWidth(1);
$pdf -> Rect(133, 277, 65, 13);
$pdf -> SetFont('Arial', 'B', 9);
$pdf -> Text(134, 281, "Numero di Casa:");
$pdf -> SetFont('Arial', '', 9);
$pdf -> Text(162, 281, $_POST['tel1']);
$pdf -> SetFont('Arial', 'B', 9);
$pdf -> Text(134, 287, "Numero di Cell.:");
$pdf -> SetFont('Arial', '', 9);
$pdf -> Text(162, 287, $_POST['tel2']);

// Mostro a video il pdf
$pdf -> Output();

// SPEDISCO IN ALLEGATO AD UNA MAIL IL PDF
// Salvo come str per allegare alla mail e spedirla
$str = $pdf -> Output('', 'S');	// Salvo la stringa del pdf

if(isset($_POST['email'])) {
    $date = $_POST['garaDel'] ? $_POST['garaDel'] : date('d_m_Y');
    $societa1 = $_POST['societa1'] ? $_POST['societa1'] : 'societa1';
    $societa2 = $_POST['societa2'] ? $_POST['societa2'] : 'societa2';
    $m = new Mail();
    $m->addTo($_POST['email']);
    $m->setFrom("noreply@aiabrescia.com", "AIA BRESCIA");
    $m->setSubject("Rapporto del $date della gara $societa1 - $societa2");
    $text = "Caro collega,<br><br>";
    $text .= "In allegato il rapporto da te compilato col servizio <b>AIAMakeReport</b> ";
    $text .= "di <a href='http://www.aiabrescia.com'>aiabrescia.com</a> ";
    $text .= "della gara $societa1 - $societa2<br><br><br>";
    $text .= "<i>AIA Brescia WebMaster</i>";
    $m->setBodyHtml($text);

    $fileName = "{$date}_{$societa1}_$societa2.pdf";
    file_put_contents($fileName, $str);
    $m->addAttachment($fileName);
    $m->send();
    unlink($fileName);
}