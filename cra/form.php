<style type="text/css">
    input[type=text] {
        text-align:center;
        background:#dddddd;
    }
    table td {
        padding: 3px 1px;
    }
</style>

<?php

function inputText($name, $top, $left, $width, $placeholder = '') {
    return "<input type=\"text\" name=\"$name\" id=\"$name\" style=\"position:absolute;top:{$top}px;left:{$left}px;width:{$width}px;\" autocomplete=\"off\" placeholder=\"$placeholder\" />";
}

function textarea($name, $top, $left, $width, $height, $placeholder = '') {
    return "<textarea name=\"$name\" id=\"$name\" style=\"position:absolute;top:{$top}px;left:{$left}px;width:{$width}px;height:{$height}px;background:#dddddd\"  placeholder=\"$placeholder\"></textarea>";
}

?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="../css/reset.css" />
<link rel="stylesheet" href="css/layout.css" />

<img src="img/cra_page1.jpg" width="1000" />
<img src="img/cra_page2.jpg" width="1000" />

<form action="export.php" target="_new" method="post" enctype="multipart/form-data">

<!-- Intestazioni -->
<?=inputText('campionato', 95, 140, 270, '')?>
<?=inputText('girone', 95, 520, 50, '')?>

<?=inputText('nome', 140, 250, 250, 'Mario, Bianchi')?>
<div style="position:absolute; top:145px; left:860px;">Brescia</div>

<!-- Dettagli burocratici della partita -->
<?=inputText('gara', 185, 90, 400, '')?>
<?=inputText('garaDel', 185, 650, 100, date('d/m/Y'))?>
<?=inputText('garaOre', 185, 850, 100, date('H:i'))?>

<?=inputText('disputata', 230, 140, 350, '')?>
<?=inputText('campo', 230, 600, 350, '')?>

<!-- Nomi e risultati delle squadre -->
<?=inputText('societa1', 305, 200, 250, '')?>
<?=inputText('nReti1', 305, 525, 50, '')?>
<?=inputText('lettReti1', 305, 580, 80, '')?>

<?=inputText('societa2', 365, 200, 250, '')?>
<?=inputText('nReti2', 365, 525, 50, '')?>
<?=inputText('lettReti2', 365, 580, 80, '')?>

<?=textarea('listaReti_1T', 450, 110, 230, 50, 'Nessuna')?>
<?=textarea('listaReti_2T', 450, 430, 230, 50, 'Nessuna')?>


<!-- Caso in cui partita sospesa o non iniziata -->
<?=textarea('nonIniziata', 325, 680, 280, 75, 'Nessuna')?>

<?=inputText('sospesaAl', 410, 775, 30, '')?>
<?=inputText('sospesaTempo', 410, 860, 30, '')?>
<?=textarea('sospesa', 435, 680, 280, 55, 'Nessuna')?>

<!-- Orari della partita -->
<?=inputText('inizio', 520, 200, 100, '')?>
<?=inputText('riposo', 550, 200, 100, '')?>
<?=inputText('fine', 580, 200, 100, '')?>

<?=inputText('rec_1T', 520, 730, 50, '')?>
<?=inputText('recPer_1T', 520, 865, 100, '')?>
<?=inputText('rec_2T', 575, 730, 50, '')?>
<?=inputText('recPer_2T', 575, 865, 100, '')?>


<!-- Sostituzioni -->
<!-- Squadra 1 -->
<?
for($i=0 ; $i<5 ; $i++)
{
	?>
<table style="position:absolute; top:<? echo 635 + ($i * 50) ?>px; left:40px;" cellpadding="0" cellspacing="0">
<tr>
	<td>
        al
        <input type="text" name="soc1_sostitAl<? echo $i ?>" style="width: 30px" />
        del
        <input type="text" name="soc1_sostitDel<? echo $i ?>" style="width: 30px" />
        t.
    </td>
    <td>esce n.</td>
    <td><input type="text" name="soc1_sostitEsce<? echo $i ?>" style="width: 270px" /></td>
</tr>
<tr>
	<td></td>
    <td>entra n.</td>
    <td><input type="text" name="soc1_sostitEntra<? echo $i ?>" style="width: 270px" /></td>
</table>
	<?
}
?>

<!-- Squadra 2 -->
<?
for($i=0 ; $i<5 ; $i++)
{
	?>
<table style="position:absolute; top:<? echo 635 + ($i * 50) ?>px; left:520px;" cellpadding="0" cellspacing="0">
<tr>
	<td>
        al
        <input type="text" name="soc2_sostitAl<? echo $i ?>" style="width: 30px" />
        del
        <input type="text" name="soc2_sostitDel<? echo $i ?>" style="width: 30px" />
        t.
    </td>
	<td>esce n.</td>
    <td><input type="text" name="soc2_sostitEsce<? echo $i ?>" style="width: 270px" /></td>
</tr>
<tr>
	<td></td>
    <td>entra n.</td>
    <td><input type="text" name="soc2_sostitEntra<? echo $i ?>" style="width: 270px" /></td>
</table>
	<?
}
?>

<!-- Persone ammesse sul terreno di gioco -->
<?=inputText('dirig_soc1', 970, 230, 250, '')?>
<?=inputText('medico_soc1', 1003, 230, 250, '')?>
<?=inputText('allen_soc1', 1033, 230, 250, '')?>
<?=inputText('massag_soc1', 1063, 230, 250, '')?>

<?=inputText('dirig_soc2', 970, 720, 250, '')?>
<?=inputText('medico_soc2', 1003, 720, 250, '')?>
<?=inputText('allen_soc2', 1033, 720, 250, '')?>
<?=inputText('massag_soc2', 1063, 720, 250, '')?>

<!-- Misure d'ordine -->
<?=textarea('misureOrdine', 1131, 40, 900, 40, 'A cura della società ospitante')?>

<!-- Comportamento dirigenti -->
<?=textarea('comportDirig', 1210, 40, 900, 200, 'Normale')?>

<!-- Comportamento del pubblico -->
<?=textarea('comportPubb', 1480, 40, 900, 95, 'Normale')?>

<!-- Espulsi -->
<?=textarea('espulsi', 1645, 40, 900, 320, 'Nessuno')?>

<!-- Ammoniti -->
<?=textarea('ammoniti', 2060, 40, 900, 320, 'Nessuno')?>

<!-- Varie -->
<?=textarea('varie', 2430, 40, 900, 60, 'Nessuna')?>

<!-- Assistente 1 -->
<?=inputText('assistente1', 2540, 230, 250, 'Nome, Cognome (Società 1)')?>
<?=inputText('assistente2', 2615, 230, 250, 'Nome, Cognome (Società 2)')?>

<input type="file" name="firma" style="position:absolute;top:2610px;left:635px;background: #ff6666;padding:20px 5px" />

<!-- Telefono -->
<?=inputText('tel1', 2737, 780, 155, '')?>
<?=inputText('tel2', 2765, 780, 155, '')?>

<p style="text-align: center;padding: 40px 0;">
    Riempi il campo seguente se vuoi ricevere il rapporto via email:<br /><br />
    <input type="text" name="email" placeholder="mia@email.it" />
<p>

<br /><br />
<center><input type="submit" name="generatePDF" value="Genera PDF" /></center>
<br /><br />

</form>
